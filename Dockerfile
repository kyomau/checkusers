FROM python:3.8.1-alpine

RUN adduser --disabled-password --gecos '' app
WORKDIR /home/app
COPY . .
RUN python -m pip install -r requirements.txt
USER app
ENTRYPOINT ["python","-u","checkinactiveusers.py"]
