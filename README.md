## Chequear Inactividad de Usuarios en AWS

Este script sirve para realizar una revision de los usuarios de IAM e identificar los usuarios 
que tienen un periodo mayor a 90 dias de inactividad, el chequeo se realiza tanto al acceso por consola 
como por access key.
