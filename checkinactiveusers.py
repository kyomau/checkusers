#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import  boto3
import  json
import  time
import  datetime
import  sys
import  os


'''

print (r"""

.___                      __  .__                ____ ___                           
|   | ____ _____    _____/  |_|__|__  __ ____   |    |   \______ ___________  ______
|   |/    \\__  \ _/ ___\   __\  \  \/ // __ \  |    |   /  ___// __ \_  __ \/  ___/
|   |   |  \/ __ \\  \___|  | |  |\   /\  ___/  |    |  /\___ \\  ___/|  | \/\___ \ 
|___|___|  (____  /\___  >__| |__| \_/  \___  > |______//____  >\___  >__|  /____  >
         \/     \/     \/                   \/               \/     \/           \/ 

  LICENSE MIT
     """)

               '''

aws_key = os.environ['AWS_KEY']
secret_key = os.environ['SECRET_KEY']


## https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html

## Habilitar para el uso con profiles.
#session = boto3.Session(profile_name='profile',region_name='region')
#client = session.client('iam')
#sns = session.client('sns')

## Habilitar para utilizar con Access Key
client = boto3.client(
'iam',region_name='us-east-1',
aws_access_key_id=aws_key,
aws_secret_access_key=secret_key)

sns = boto3.client(
'sns',region_name='us-east-1',
aws_access_key_id=aws_key,
aws_secret_access_key=secret_key)


usernames = []
userak = []

def check_users_inactivity():
    global accesskey_lastused
    accesskey_lastused = ''

    currentdate = time.strftime("%Y-%m-%d %H:%M:%S",time.gmtime())
    currentd = time.mktime(datetime.datetime.strptime(currentdate, "%Y-%m-%d %H:%M:%S").timetuple())

    users = client.list_users()
    for key in users['Users']:
        a = str(key['UserName'])
        usernames.append(a)

    users_count = len(usernames)
    users_inactive = 0

    for username in usernames:

        try:
            res = client.get_user(UserName=username)
            passwordlastused = res['User']['PasswordLastUsed']
            passwordlastused = passwordlastused.strftime("%Y-%m-%d %H:%M:%S")
       	    pwdlastused = time.mktime(datetime.datetime.strptime(passwordlastused, "%Y-%m-%d %H:%M:%S").timetuple())
            
            
        except:
               f = ' Sin acceso a consola web '
        
        pwd_days = (currentd - pwdlastused) /60/60/24
        a = str(username)
        b = str(' Ultimo acceso web: ' + passwordlastused + " - ")

        try:
            #del userak[:]
            akres = client.list_access_keys(UserName=username)

            #Si el usuario no tiene AccessKey
            if (akres['AccessKeyMetadata'] == []) : 
                accesskey_lastused = "Sin AccessKey"
                noak_data = a + b + accesskey_lastused

                if ( pwd_days > 90 ) : 
                    userak.append(noak_data)
                    users_inactive +=1


            for var in akres['AccessKeyMetadata']:
              
              ak_id = str(var['AccessKeyId'])
              ak_status = var['Status']

              try:
                  if (ak_status == 'Active') :
                        aklastres = client.get_access_key_last_used(AccessKeyId=ak_id)
                        accesskey_lastused = aklastres['AccessKeyLastUsed']['LastUsedDate']
                        accesskey_lastused = accesskey_lastused.strftime("%Y-%m-%d %H:%M:%S")
                  else :
                      accesskey_lastused = 'Inactiva'

              except:
                  accesskey_lastused = 'N/A'
              
                         
              c = str(' Ultimo acceso con AccessKeyId: ' + ak_id + " el: " + accesskey_lastused)

              ak_data = a + b + c

              if (accesskey_lastused != 'Inactiva' | accesskey_lastused != 'N/A') :
                    ak_days = time.mktime(datetime.datetime.strptime(accesskey_lastused, "%Y-%m-%d %H:%M:%S").timetuple())

              if ( pwd_days > 90 & ak_days > 90) :
                    userak.append(ak_data)
                    users_inactive +=1
              
              
        except:
            accesskey_lastused = 'No tiene access key'
        
        

    userak.insert(0,'Cantidad de usuarios: ' + str(users_count))
    userak.insert(1,'Cantidad de usuarios Inactivos: ' + str(users_inactive))
    print('\n'.join(userak))
    list = '\n'.join(userak)

    sns_message = (list)
    topic_arn = 'arn:aws:sns:us-east-1:xxxxxxxxxxxxxxxxxxxxxxxxxxx' #Ingresar Topic

    response = sns.publish(TopicArn=topic_arn, Message = sns_message)

check_users_inactivity()
